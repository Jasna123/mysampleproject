<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Form</title>
<!-- <link rel="stylesheet" href="https://examples.javacodegeeks.com/wp-content/litespeed/localres/maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<script
src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet"
href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> -->
<style>
.error {
	color: red
}
</style>
</head>
<body>
<div align="center">
<div></div>
 <table>
 <tr> <h2>Customer Registration Form</h2></tr>
 <h4>Please fill the below details<h4>
<form:form action="processForm" modelAttribute="customer">
<tr>
<td>
First Name(*) : </td><td><form:input path="firstName"/></td><td>
<form:errors path="firstName" cssClass="error">
</form:errors></td><td>
</td>
</tr>
<tr>
<td>
Last Name(*) :  </td><td><form:input path="lastName"/></td><td>
<form:errors path="lastName" cssClass="error">
</form:errors></td><td>
</td>
</tr>
<tr>
<td>
Gender(*) : </td><td>
Male : <form:radiobutton path="gender" value="Male"/>
Female : <form:radiobutton path="gender" value="Female"/></td><td>
<form:errors path="gender" cssClass="error"></form:errors></td><td>
</td>
</tr>
<tr>
<td>
City Name(*) : </td><td><form:input path="city"/></td><td>
<form:errors path="city" cssClass="error"></form:errors></td><td>
</td>
</tr>
<tr>
<td>
PinCode(*) :</td><td> <form:input path="pincode"/></td><td>
<form:errors path="pincode" cssClass="error"></form:errors></td><td>
</td>
</tr>
<tr>
<td>
Phone Number :</td><td> <form:input path="mobnum"/></td><td>
</td>
</tr>
<tr>
<td>
Email (*): </td><td><form:input path="email"/></td><td>
<form:errors path="email" cssClass="error"></form:errors></td><td>
</td>
</tr>
<tr>
<td>
Number of Free Passes (*): </td><td><form:input path="freepasses"/></td><td>
<form:errors path="freepasses" cssClass="error"></form:errors></td><td>
</td>
</tr>
<tr>
<td>
Subject :</td><td> <form:input path="subject"/></td><td>
</td>
</tr>
<tr>
<td>
Query/Complaint :</td><td> <form:textarea path="qcmessage"/></td><td>
</td>
</tr>
<tr>
<td colspan="2" align="center"><input type="submit" value="Submit"></td>
</tr>
</form:form>
</table>
</div>
</body>
</html>
